# La prison

Bot discord en python qui permet de gérer une "prison" sur un serveur.

# Préparation :
- Il faut commencer par créer un channel dédié pour la prison
- Il faut ensuite créer un rôle de prisonnier avec aucun droits (tout décocher ou laisser l'affichage séparé et les mentions)
- Il faut aussi créer un rôle de gardien de prison
- Ensuite il faut configurer les catégorie du serveur discord pour donner les droits voulus aux prisonniers (lire et voir les anciens messages en général)
- Puis il faut synchroniser chaque channel avec la catégorie pour ne pas tout refaire à la main
- Enfin il faut configurer le channel dédié à la prison pour que les prisonnier puissent avoir les droits normaux (écriture obligatoire)
- Pour finir il faut ajouter le bot sur le serveur et surtout : mettre son rôle tout en haut de la hiérarchie !

# Configuration :
Il faut configurer le fichier config.json :
- Ne pas toucher à la valeur de "discord_token"
- Copier coller un objet contenu dans "guilds"
- Nommer le serveur
- Remplacer la valeur de "id" avec l'id du serveur (clic droit sur le serveur -> copier l'identifiant tout en bas)
- Remplacer la valeur de "jail_channel_id" par l'id du channel dédié pour la prison (clic droit sur le channel : copier l'identifiant, tout en bas de la liste)
- Remplacer la valeur de "prisonner_role_id" par l'id du rôle des prisonniers (Paramètres du serveur -> Rôles -> clic droit sur le rôle choisi -> copier l'identifiant)
- Remplacer la valeur de "guard_role_id" par l'id du rôle des gardiens de la prison (Paramètres du serveur -> Rôles -> clic droit sur le rôle choisi -> copier l'identifiant)
- Mettre la durée de l'intervalle voulue entre deux lancés de dé en secondes dans "trys_interval"
- Mettre le nombre de faces voulues pour le dé dans "dice"
- Choisir un template de strings dans strings.json et mettre le nom dans "strings"