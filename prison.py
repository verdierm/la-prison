import discord
from discord.utils import get
from datetime import datetime
import random
import json
import sys
import os

'''
Sauvegarde les modification de la configuration dans le fichier de configuration
'''
def saveConfig(guild):
    for i in range(len(config['guilds'])):
        if config['guilds'][i]['id'] == guild['id']:
            config['guilds'][i] = guild
    with open(config_file, 'w+') as file:
        file.write(json.dumps(config, indent=4))

"""
Transforme un fichier json en dictionnaire avec des clés int au lieu de str
"""
def pythonify(json_data):
    for key, value in json_data.items():
        if isinstance(value, list):
            value = [ pythonify(item) if isinstance(item, dict) else item for item in value ]
        elif isinstance(value, dict):
            value = pythonify(value)
        try:
            newkey = int(key)
            del json_data[key]
            key = newkey
        except ValueError:
            pass
        json_data[key] = value
    return json_data

class Client(discord.Client):

    async def on_ready(self):
        print(f'{self.user.name} a enfin réussi à se connecter à discord!')
        self.prisonners = self.retrieve_data(self.sync_prisonners)
        print('prisonners:')
        print(self.prisonners)
        self.giffers = self.retrieve_data(self.sync_giffers)
        print('giffers:')
        print(self.giffers)

    '''
    Gestion de tous les messages écris sur le channel dédié
    '''
    async def on_message(self, message):
        if message.author.id == self.user.id:
            return
        guild = {}
        with open(config_file) as file:
            config = json.load(file)
        with open(string_file, encoding='utf-8') as file:
            all_strings = json.load(file)
        for g in config['guilds']:
            if g['id'] == message.guild.id:
                guild = g
        if guild == {}:
            return
        strings = all_strings[guild['strings']]
        if message.guild.id not in self.prisonners.keys():
            self.prisonners[message.guild.id] = {}
        if message.channel.id == guild['jail_channel_id']:
            commands = self.setCommands(strings)
            tokens = message.content.split()
            if len(tokens) > 0 and tokens[0] in commands:
                print(f'{datetime.now().strftime("[%d/%m/%Y - %H:%M:%S]")} {message.guild.name}, {message.channel.name}, {message.author.name}, {message.content}')
                await commands[tokens[0]]['func'](message, guild, tokens, strings, commands)
            elif len(tokens) > 0 and tokens[0][0] == '!':
                await message.channel.send(strings['bad_cmd'])
        await self.checkGif(message, guild, strings)

    '''
    Set commands list with guild's strings set
    '''
    def setCommands(self, strings):
        commands = {}
        commands[strings['help_cmd']] = {'func' : self.listCommands, 'usage' : strings['help_cmd']}
        commands[strings['to_jail_cmd']] = {'func' : self.toJail, 'usage' : strings['to_jail_usage']}
        commands[strings['exit_jail_cmd']] = {'func' : self.tryToExit, 'usage' : strings['exit_jail_cmd']}
        commands[strings['grace_cmd']] = {'func' : self.grace, 'usage' : strings['grace_usage']}
        commands[strings['interval_cmd']] = {'func' : self.changeInterval, 'usage' : strings['interval_usage']}
        commands[strings['dice_cmd']] = {'func' : self.changeDice, 'usage' : strings['dice_usage']}
        commands[strings['max_jail_cmd']] = {'func' : self.changeMaxJailTime, 'usage' : strings['max_jail_usage']}
        commands[strings['max_gifs_cmd']] = {'func' : self.changeMaxGifs, 'usage' : strings['max_gifs_usage']}
        commands[strings['gifs_interval_cmd']] = {'func' : self.changeGifsInterval, 'usage' : strings['gifs_interval_usage']}
        return commands

    '''
    Liste toutes les commandes supportées par le bot et la notice d'utilisation
    '''
    async def listCommands(self, message, guild, tokens, strings, commands):
        cmds = strings['list_cmds'] + '\n'
        for key in commands.keys():
            cmds += commands[key]['usage'] + '\n'
        await message.channel.send(cmds)

    '''
    Envoi un membre dans la prison si c'est possible
    Stocke ses rôles et le timestamp actuel
    Stocke ces infos dans un fichier en cas de problèmes
    Enlève tous les rôles du membre et lui donne le rôle de prisonnier
    '''
    async def toJail(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        userId = tokens[1].replace('<', '').replace('>', '').replace('@', '').replace('!', '')
        try:
            userId = int(userId)
        except ValueError:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        user = await message.guild.fetch_member(userId)
        author = await message.guild.fetch_member(int(message.author.id))
        if not user:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        if user.id == self.user.id:
            await message.channel.send(f'{user.display_name} {strings["cannot_be_jailed"]}')
            return
        if user.id in self.prisonners[message.guild.id].keys():
            await message.channel.send(f'{user.display_name} {strings["already_jailed"]}')
            return
        try:
            await user.edit(nick=user.nick)
        except:
            await message.channel.send(f'{user.display_name} {strings["cannot_be_jailed"]}')
            return
        await self.jailling(message, user, guild, strings['just_jailed'], strings, message.channel)

    '''
    Lance un dé si l'intervalle imposée est passée
    Le nombre maximum du dé libère le membre qui a lancé le dé
    N'importe quel autre nombre remet à 0 le timestamp
    '''
    async def tryToExit(self, message, guild, tokens, strings, commands):
        if message.author.id not in self.prisonners[message.guild.id].keys():
            await message.channel.send(strings['not_jailed'])
            return
        enter = datetime.timestamp(datetime.now()) - self.prisonners[message.guild.id][message.author.id]['enter']
        time = datetime.timestamp(datetime.now()) - self.prisonners[message.guild.id][message.author.id]['time']
        user = await message.guild.fetch_member(int(message.author.id))
        mult = self.prisonners[message.guild.id]['recidives'][user.id]['number']
        if int(enter) / 60 >= guild['max_jail_time'] * mult:
            await self.remove_prisonner(user, message.guild)
            await message.channel.send(f'{strings["max_jail_time_passed"]} {message.author.mention}')
            return
        if time < guild['trys_interval'] * mult:
            time = self.getStrTime(int((guild['trys_interval'] * mult) - time), strings)
            await message.channel.send(f'{strings["retry_interval"]} {time}')
            return
        dice = random.randint(1, guild['dice'])
        result = ''
        if dice == guild['dice']:
            result = strings['good_dice']
            await self.remove_prisonner(user, message.guild)
        else:
            result = strings['bad_dice']
            self.prisonners[message.guild.id][user.id]['time'] = datetime.timestamp(datetime.now())
            with open(self.sync_prisonners, 'w+') as file:
                file.write(json.dumps(self.prisonners, indent=4))
        await message.channel.send(f'{strings["result"]} {dice}, {result}')

    '''
    Permet de gracier un membre emprisonné
    '''
    async def grace(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        userId = tokens[1].replace('<', '').replace('>', '').replace('@', '').replace('!', '')
        try:
            userId = int(userId)
        except ValueError:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        user = await message.guild.fetch_member(userId)
        author = await message.guild.fetch_member(int(message.author.id))
        if not user:
            await message.channel.send(f'{strings["usage"]} {commands[tokens[0]]["usage"]}')
            return
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        if user.id not in self.prisonners[message.guild.id].keys():
            await message.channel.send(f'{user.display_name} {strings["not_jailed_target"]}')
            return
        await self.remove_prisonner(user, message.guild)
        msg = f'{message.author.mention} {strings["grace"]} {tokens[1]} !'
        await message.channel.send(msg)

    async def jailling(self, message, user, guild, jail_string, strings, channel):
        self.prisonners[message.guild.id][user.id] = {}
        self.prisonners[message.guild.id][user.id]['roles'] = [role.id for role in user.roles]
        self.prisonners[message.guild.id][user.id]['time'] = datetime.timestamp(datetime.now())
        self.prisonners[message.guild.id][user.id]['enter'] = datetime.timestamp(datetime.now())
        if not 'recidives' in self.prisonners[message.guild.id].keys():
            self.prisonners[message.guild.id]['recidives'] = {}
        if not user.id in self.prisonners[message.guild.id]['recidives'].keys():
            self.prisonners[message.guild.id]['recidives'][user.id] = {}
            self.prisonners[message.guild.id]['recidives'][user.id]['number'] = 1
        elif int(datetime.timestamp(datetime.now()) - self.prisonners[message.guild.id]['recidives'][user.id]['last']) / 3600 >= guild['recidives_reset_time']:
            self.prisonners[message.guild.id]['recidives'][user.id]['number'] = 1
        else:
            self.prisonners[message.guild.id]['recidives'][user.id]['number'] = self.prisonners[message.guild.id]['recidives'][user.id]['number'] + 1
        self.prisonners[message.guild.id]['recidives'][user.id]['last'] = datetime.timestamp(datetime.now())
        with open(self.sync_prisonners, 'w+') as file:
            file.write(json.dumps(self.prisonners, indent=4))
        prisonner_role = get(message.guild.roles, id=guild['prisonner_role_id'])
        new_roles = user.roles[0:1]
        new_roles.append(prisonner_role)
        await user.edit(roles=new_roles)
        mult = self.prisonners[message.guild.id]['recidives'][user.id]['number']
        interval = self.getStrTime(guild['trys_interval'] * mult, strings)
        dice = guild['dice']
        max_jail_time = self.getStrTime((guild['max_jail_time'] * mult) * 60, strings)
        await channel.send(f'{jail_string} {user.mention} ! {strings["info_interval"]} {interval}, {strings["info_dice"]} {dice}, {strings["info_max_jail_time"]} {max_jail_time}.')

    '''
    Sort un prisonnier de prison
    Lui enlève le rôle de prisonnier et lui rmet ses anciens rôles
    Le retire du fichier de synchronisation
    '''
    async def remove_prisonner(self, user, guild):
        roles = []
        for role_id in self.prisonners[guild.id][user.id]['roles']:
            roles.append(get(guild.roles, id=int(role_id)))
        await user.edit(roles=roles)
        self.prisonners[guild.id].pop(user.id)
        with open(self.sync_prisonners, 'w+') as file:
            file.write(json.dumps(self.prisonners, indent=4))
        if guild.id in self.giffers.keys() and user.id in self.giffers[guild.id].keys():
            self.giffers[guild.id].pop(user.id)

    '''
    Permet d'afficher ou de modifier l'intervalle entre deux lancés de dé
    '''
    async def changeInterval(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            time = self.getStrTime(guild['trys_interval'], strings)
            await message.channel.send(f'{strings["actual_interval"]} {time}')
            return
        author = await message.guild.fetch_member(int(message.author.id))
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        time = int(tokens[1])
        if time < 0:
            time = 0
        guild['trys_interval'] = time
        time = self.getStrTime(time, strings)
        saveConfig(guild)
        await message.channel.send(f'{strings["interval_changed"]} {time}')

    '''
    Permet d'afficher ou de modifier le nombre de face du dé
    '''
    async def changeDice(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            await message.channel.send(f'{strings["actual_dice"]} {guild["dice"]} {strings["faces"]}')
            return
        author = await message.guild.fetch_member(int(message.author.id))
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        dice = int(tokens[1])
        if dice < 2:
            dice = 6
        guild['dice'] = dice
        saveConfig(guild)
        await message.channel.send(f'{strings["dice_changed"]} {dice} {strings["faces"]}')

    """
    Permet d'afficher ou modifier le temps maximum d'emprisonnement
    """
    async def changeMaxJailTime(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            await message.channel.send(f'{strings["actual_max_jail_time"]} {self.getStrTime(guild["max_jail_time"] * 60, strings)}')
            return
        author = await message.guild.fetch_member(int(message.author.id))
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        time = int(tokens[1])
        if time < 0:
            time = 0
        guild['max_jail_time'] = time
        time = self.getStrTime(time * 60, strings)
        saveConfig(guild)
        await message.channel.send(f'{strings["max_jail_time_changed"]} {time}')

    """
    Permet d'afficher ou modifier le nombre maxium de gifs autorisés pendant l'intervalle
    """
    async def changeMaxGifs(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            await message.channel.send(f'{strings["actual_max_gifs"]} {guild["max_gifs"]}')
            return
        author = await message.guild.fetch_member(int(message.author.id))
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        gifs = int(tokens[1])
        if gifs < 3:
            gifs = 3
        guild['max_gifs'] = gifs
        saveConfig(guild)
        await message.channel.send(f'{strings["max_gifs_changed"]} {gifs}')

    """
    Permet d'afficher ou de modifier l'intervalle entre le 1er et le dernier gifs avant emprisonnement
    """
    async def changeGifsInterval(self, message, guild, tokens, strings, commands):
        if len(tokens) < 2:
            time = self.getStrTime(guild['gifs_interval'], strings)
            await message.channel.send(f'{strings["actual_gifs_interval"]} {time}')
            return
        author = await message.guild.fetch_member(int(message.author.id))
        if message.author.id in self.prisonners[message.guild.id].keys() or (guild['guard_role_id'] != 0 and not get(author.roles, id=guild['guard_role_id'])):
            await message.channel.send(strings['no_rights'])
            return
        time = int(tokens[1])
        if time < 0:
            time = 30
        guild['gifs_interval'] = time
        time = self.getStrTime(time, strings)
        saveConfig(guild)
        await message.channel.send(f'{strings["gifs_interval_changed"]} {time}')

    '''
    Au cas où le bot a crash pendant que des membres étaient en prison
    Récupère les infos de ces membre (id, rôles, timestamp)
    '''
    def retrieve_data(self, sync_file):
        with open(sync_file, 'a+') as file:
            file.seek(0)
            try:
                data = pythonify(json.load(file))
                return data
            except:
                file.seek(0)
                file.truncate(0)
                file.write(json.dumps({}, indent=4))
                return ({})

    '''
    Transforme une opération de timestamps en string heures/minutes/secondes
    '''
    def getStrTime(self, time, strings):
        seconds = time % 60
        minutes = 0 if time < 60 else int((time % 3600) / 60)
        hours = 0 if time < 3600 else int((time - (time % 3600)) / 3600)
        time = ''
        if hours > 0:
            time += f'{hours} {strings["hours"]} '
        if minutes > 0:
            time += f'{minutes} {strings["minutes"]} '
        time += f'{seconds} {strings["seconds"]}'
        return time

    '''
    Vérifie si le message est un gif et détermine si c'est du spam
    Envoie l'auteur en prison si l'anti spam est actif sur le serveur
    '''
    async def checkGif(self, message, guild, strings):
        if guild['anti_gifs_spam'] is False:
            return
        for source in self.gifs_sources:
            if not message.content.startswith(source):
                continue
            if message.guild.id not in self.giffers.keys():
                self.giffers[guild['id']] = {}
            if message.author.id not in self.giffers[guild['id']].keys():
                self.giffers[guild['id']][message.author.id] = {}
            if message.channel.id not in self.giffers[guild['id']][message.author.id].keys():
                self.giffers[guild['id']][message.author.id][message.channel.id] = [datetime.timestamp(datetime.now())]
                print(f'{datetime.now().strftime("[%d/%m/%Y - %H:%M:%S]")} {message.guild.name}, {message.channel.name}, {message.author.name}, {source}, 1 (max: {guild["max_gifs"]}), 0')
                with open(self.sync_giffers, 'w+') as file:
                    file.write(json.dumps(self.giffers, indent=4))
                return
            gifs = self.giffers[guild['id']][message.author.id][message.channel.id]
            if len(gifs) >= guild['max_gifs']:
                gifs = gifs[1:] + gifs[:1]
                gifs[len(gifs) - 1] = datetime.timestamp(datetime.now())
            else:
                gifs.append(datetime.timestamp(datetime.now()))
            print(f'{datetime.now().strftime("[%d/%m/%Y - %H:%M:%S]")} {message.guild.name}, {message.channel.name}, {message.author.name}, {source}, {len(gifs)} (max: {guild["max_gifs"]}), {self.getStrTime(int(gifs[len(gifs) - 1] - gifs[0]), strings)}')
            with open(self.sync_giffers, 'w+') as file:
                file.write(json.dumps(self.giffers, indent=4))
            if len(gifs) >= guild['max_gifs'] and gifs[len(gifs) - 1] - gifs[0] <= guild['gifs_interval']:
                user = get(message.guild.members, id=message.author.id)
                if user.id in self.prisonners[message.guild.id].keys():
                    return
                try:
                    await user.edit(nick=user.nick)
                except:
                    return
                jail = self.get_channel(int(guild['jail_channel_id']))
                await self.jailling(message, user, guild, strings['gifs_spam'], strings, jail)

    prisonners = {}
    giffers = {}
    sync_prisonners = 'sync_prisonners.json'
    sync_giffers = 'sync_giffers.json'
    gifs_sources = ['https://giphy.com', 'https://tenor.com']

'''
Récupération des configs et des templates de strings
Lancement du bot avec le token généré sur le portail développeurs de discord
'''

path = os.path.dirname(os.path.realpath(__file__))

config_file = path + '/config.json'
string_file = path + '/strings.json'
with open(config_file) as file:
    config = json.load(file)
with open(string_file, encoding='utf-8') as file:
    all_strings = json.load(file)
client = Client()
client.run(config['discord_token'])
