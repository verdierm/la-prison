#!/bin/bash

bot_name="prison"
bot_dir="."

function start() {
	if [[ "$(screen -ls $bot_name)" =~ "There is a screen on:" ]]; then
		echo  "Please stop $bot_name before starting again"
		exit
	fi

	screen -dmS $bot_name -L -Logfile logs/$bot_name.log python3 $bot_dir/$bot_name.py
	screen -S $bot_name -X logfile flush 0
	
	if [[ "$(screen -ls $bot_name)" =~ "There is a screen on:" ]]; then
		echo  "$bot_name is now running"
	fi
}

function stop() {
	if [[ "$(screen -ls $bot_name)" =~ "There is a screen on:" ]]; then
		screen -X -S $bot_name quit
		echo "$bot_name is now stopped"
	else
		echo "$bot_name is not running"
	fi
}

function status() {
	if [[ "$(screen -ls $bot_name)" =~ "There is a screen on:" ]]; then
		echo "$bot_name is running"
	else
		echo "$bot_name is not running"
	fi
}

case "$1" in
	start)
		start
		;;

	stop)
		stop
		;;

	status)
		status
		;;

	restart)
		stop
		start
		;;

	*)
		echo $"Usage: $0 {start|stop|restart|status}"
		exit 1

	esac
